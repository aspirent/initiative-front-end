# README #
## Initiative Front End ##

This Repository includes HTML, CSS, and Javascript for the Initiative site's front end

### Requirements: ###

* bootstrap 3.3.6 (included locally)
* jasny-bootstrap 3.1.3 (included locally)
* font-awesome 4.5.0 (included locally)
* JQuery 1.10.2 (included remotely at: https://code.jquery.com/jquery-1.10.2.min.js)

### Basics Of Setup And Use  ###

* look in the Example Pages folder to open some examples

#### Build By Caleb Adams ####
#### <CalebAshmoreAdams@gmail.com> ####
